/*
 * Copyright (C) 2020 ChromaWay AB. See LICENSE for license information.
 */

package net.postchain.rell.model

import net.postchain.rell.runtime.Rt_CallFrame
import net.postchain.rell.runtime.Rt_StackTraceError
import net.postchain.rell.runtime.Rt_TupleValue
import net.postchain.rell.runtime.Rt_Value

sealed class R_StatementResult
class R_StatementResult_Return(val value: Rt_Value?): R_StatementResult()
class R_StatementResult_Break: R_StatementResult()

abstract class R_Statement {
    abstract fun execute(frame: Rt_CallFrame): R_StatementResult?
}

object R_EmptyStatement: R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        return null
    }
}

sealed class R_VarDeclarator {
    abstract fun initialize(frame: Rt_CallFrame, value: Rt_Value, overwrite: Boolean)
}

class R_SimpleVarDeclarator(val ptr: R_VarPtr, val type: R_Type, val adapter: R_TypeAdapter): R_VarDeclarator() {
    override fun initialize(frame: Rt_CallFrame, value: Rt_Value, overwrite: Boolean) {
        val value2 = adapter.adaptValue(value)
        frame.set(ptr, type, value2, overwrite)
    }
}

class R_TupleVarDeclarator(val subDeclarators: List<R_VarDeclarator>): R_VarDeclarator() {
    override fun initialize(frame: Rt_CallFrame, value: Rt_Value, overwrite: Boolean) {
        val tuple = value.asTuple()
        for ((i, declarator) in subDeclarators.withIndex()) {
            val subValue = tuple[i]
            declarator.initialize(frame, subValue, overwrite)
        }
    }
}

object R_WildcardVarDeclarator: R_VarDeclarator() {
    override fun initialize(frame: Rt_CallFrame, value: Rt_Value, overwrite: Boolean) {
        // Do nothing.
    }
}

class R_VarStatement(val declarator: R_VarDeclarator, val expr: R_Expr?): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        if (expr != null) {
            val value = expr.evaluate(frame)
            declarator.initialize(frame, value, false)
        }
        return null
    }
}

class R_ReturnStatement(val expr: R_Expr?): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val value = expr?.evaluate(frame)
        return R_StatementResult_Return(value)
    }
}

class R_BlockStatement(val stmts: List<R_Statement>, val frameBlock: R_FrameBlock): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val res = frame.block(frameBlock) {
            executeStatements(frame, stmts)
        }
        return res
    }

    companion object {
        fun executeStatements(frame: Rt_CallFrame, stmts: List<R_Statement>): R_StatementResult? {
            for (stmt in stmts) {
                val res = stmt.execute(frame)
                if (res != null) {
                    return res
                }
            }
            return null
        }

    }
}

class R_ExprStatement(private val expr: R_Expr): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        expr.evaluate(frame)
        return null
    }
}

class R_ReplExprStatement(private val expr: R_Expr): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val res = expr.evaluate(frame)
        frame.defCtx.appCtx.replOut?.printValue(res)
        return null
    }
}

class R_AssignStatement(val dstExpr: R_DestinationExpr, val expr: R_Expr, val op: R_BinaryOp?): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val dstRef = dstExpr.evaluateRef(frame)
        dstRef ?: return null // Null-safe access (operator ?.).

        val value = if (op != null) {
            val left = dstRef.get()
            val right = expr.evaluate(frame)
            op.evaluate(left, right)
        } else {
            expr.evaluate(frame)
        }

        dstRef.set(value)
        return null
    }
}

class R_IfStatement(val expr: R_Expr, val trueStmt: R_Statement, val falseStmt: R_Statement): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val cond = expr.evaluate(frame)
        val b = cond.asBoolean()
        val stmt = if (b) trueStmt else falseStmt
        val res = stmt.execute(frame)
        return res
    }
}

class R_WhenStatement(val chooser: R_WhenChooser, val stmts: List<R_Statement>): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val choice = chooser.choose(frame)
        val res = if (choice == null) null else stmts[choice].execute(frame)
        return res
    }
}

class R_WhileStatement(val expr: R_Expr, val stmt: R_Statement, val frameBlock: R_FrameBlock): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        while (true) {
            val cond = expr.evaluate(frame)
            val b = cond.asBoolean()
            if (!b) {
                break
            }

            val res = executeBody(frame)
            if (res != null) {
                return if (res is R_StatementResult_Return) res else null
            }
        }
        return null
    }

    private fun executeBody(frame: Rt_CallFrame): R_StatementResult? {
        return frame.block(frameBlock) {
            stmt.execute(frame)
        }
    }
}

sealed class R_ForIterator {
    abstract fun list(v: Rt_Value): Iterable<Rt_Value>
}

object R_ForIterator_Collection: R_ForIterator() {
    override fun list(v: Rt_Value): Iterable<Rt_Value> = v.asCollection()
}

object R_ForIterator_VirtualCollection: R_ForIterator() {
    override fun list(v: Rt_Value): Iterable<Rt_Value> = v.asVirtualCollection().iterable()
}

class R_ForIterator_Map(private val tupleType: R_TupleType): R_ForIterator() {
    override fun list(v: Rt_Value): Iterable<Rt_Value> {
        return v.asMap().entries.map { Rt_TupleValue(tupleType, listOf(it.key, it.value)) }
    }
}

object R_ForIterator_Range: R_ForIterator() {
    override fun list(v: Rt_Value): Iterable<Rt_Value> = v.asRange()
}

class R_ForStatement(
        val varDeclarator: R_VarDeclarator,
        val expr: R_Expr,
        val iterator: R_ForIterator,
        val stmt: R_Statement,
        val frameBlock: R_FrameBlock
): R_Statement()
{
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        val value = expr.evaluate(frame)
        val list = iterator.list(value)

        val res = frame.block(frameBlock) {
            execute0(frame, list)
        }

        return res
    }

    private fun execute0(frame: Rt_CallFrame, list: Iterable<Rt_Value>): R_StatementResult? {
        var first = true
        for (item in list) {
            varDeclarator.initialize(frame, item, !first)
            first = false
            val res = stmt.execute(frame)
            if (res != null) {
                return if (res is R_StatementResult_Return) res else null
            }
        }
        return null
    }
}

class R_BreakStatement: R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        return R_StatementResult_Break()
    }
}

class R_StackTraceStatement(private val subStmt: R_Statement, private val filePos: R_FilePos): R_Statement() {
    override fun execute(frame: Rt_CallFrame): R_StatementResult? {
        return Rt_StackTraceError.trackStack(frame, filePos) {
            subStmt.execute(frame)
        }
    }
}
