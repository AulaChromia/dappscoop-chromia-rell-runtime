/*
 * Copyright (C) 2020 ChromaWay AB. See LICENSE for license information.
 */

package net.postchain.rell.sql

import net.postchain.config.DatabaseConnector
import net.postchain.rell.runtime.Rt_Error
import org.jooq.tools.jdbc.MockConnection
import java.io.Closeable
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong

object SqlConstants {
    const val ROWID_COLUMN = "rowid"
    const val ROWID_GEN = "rowid_gen"
    const val MAKE_ROWID = "make_rowid"

    const val BLOCKS_TABLE = "blocks"
    const val BLOCKCHAINS_TABLE = "blockchains"
    const val TRANSACTIONS_TABLE = "transactions"

    val SYSTEM_OBJECTS = setOf(
            ROWID_GEN,
            MAKE_ROWID,
            BLOCKS_TABLE,
            BLOCKCHAINS_TABLE,
            TRANSACTIONS_TABLE,
            "configurations",
            "meta",
            "peerinfos"
    )
}

class SqlConnectionLogger(private val logging: Boolean) {
    private val conId = idCounter.getAndIncrement()

    fun log(s: String) {
        if (logging) println("[$conId] $s")
    }

    companion object {
        private val idCounter = AtomicLong()
    }
}

abstract class SqlManager {
    private val busy = AtomicBoolean()

    protected abstract fun <T> execute0(tx: Boolean, code: (SqlExecutor) -> T): T

    fun <T> transaction(code: (SqlExecutor) -> T): T = execute(true, code)
    fun <T> access(code: (SqlExecutor) -> T): T = execute(false, code)

    fun <T> execute(tx: Boolean, code: (SqlExecutor) -> T): T {
        check(busy.compareAndSet(false, true))
        try {
            val res = execute0(tx) { sqlExec ->
                SingleUseSqlExecutor(sqlExec).use(code)
            }
            return res
        } finally {
            check(busy.compareAndSet(true, false))
        }
    }

    private class SingleUseSqlExecutor(private val sqlExec: SqlExecutor): SqlExecutor(), Closeable {
        private var valid = true

        override fun <T> connection(code: (Connection) -> T): T {
            check(valid)
            return sqlExec.connection(code)
        }

        override fun execute(sql: String) {
            check(valid)
            sqlExec.execute(sql)
        }

        override fun execute(sql: String, preparator: (PreparedStatement) -> Unit) {
            check(valid)
            sqlExec.execute(sql, preparator)
        }

        override fun executeQuery(sql: String, preparator: (PreparedStatement) -> Unit, consumer: (ResultSet) -> Unit) {
            check(valid)
            sqlExec.executeQuery(sql, preparator, consumer)
        }

        override fun close() {
            check(valid)
            valid = false
        }
    }
}

abstract class SqlExecutor {
    abstract fun <T> connection(code: (Connection) -> T): T
    abstract fun execute(sql: String)
    abstract fun execute(sql: String, preparator: (PreparedStatement) -> Unit)
    abstract fun executeQuery(sql: String, preparator: (PreparedStatement) -> Unit, consumer: (ResultSet) -> Unit)
}

object NoConnSqlManager: SqlManager() {
    override fun <T> execute0(tx: Boolean, code: (SqlExecutor) -> T): T {
        val res = code(NoConnSqlExecutor)
        return res
    }
}

object NoConnSqlExecutor: SqlExecutor() {
    override fun <T> connection(code: (Connection) -> T): T {
        val con = MockConnection { TODO() }
        val res = code(con)
        return res
    }

    override fun execute(sql: String) = throw err()
    override fun execute(sql: String, preparator: (PreparedStatement) -> Unit) = throw err()
    override fun executeQuery(sql: String, preparator: (PreparedStatement) -> Unit, consumer: (ResultSet) -> Unit) = throw err()

    private fun err() = Rt_Error("no_sql", "No database connection")
}

class ConnectionSqlManager(private val con: Connection, logging: Boolean): SqlManager() {
    private val conLogger = SqlConnectionLogger(logging)
    private val sqlExec = ConnectionSqlExecutor(con, conLogger)

    init {
        check(con.autoCommit)
    }

    override fun <T> execute0(tx: Boolean, code: (SqlExecutor) -> T): T {
        val res = if (tx) {
            transaction0(code)
        } else {
            access0(code)
        }
        return res
    }

    private fun <T> transaction0(code: (SqlExecutor) -> T): T {
        val autoCommit = con.autoCommit
        check(autoCommit)
        try {
            con.autoCommit = false
            var rollback = true
            try {
                conLogger.log("BEGIN TRANSACTION")
                val res = code(sqlExec)
                conLogger.log("COMMIT TRANSACTION")
                con.commit()
                rollback = false
                return res
            } finally {
                if (rollback) {
                    conLogger.log("ROLLBACK TRANSACTION")
                    con.rollback()
                }
            }
        } finally {
            con.autoCommit = autoCommit
        }
    }

    private fun <T> access0(code: (SqlExecutor) -> T): T {
        check(con.autoCommit)
        val res = code(sqlExec)
        check(con.autoCommit)
        return res
    }
}

class ConnectionSqlExecutor(private val con: Connection, private val conLogger: SqlConnectionLogger): SqlExecutor() {
    constructor(con: Connection, logging: Boolean): this(con, SqlConnectionLogger(logging))

    override fun <T> connection(code: (Connection) -> T): T {
        val autoCommit = con.autoCommit
        val res = code(con)
        check(con.autoCommit == autoCommit)
        return res
    }

    override fun execute(sql: String) {
        execute0(sql) { con ->
            con.createStatement().use { stmt ->
                stmt.execute(sql)
            }
        }
    }

    override fun execute(sql: String, preparator: (PreparedStatement) -> Unit) {
        execute0(sql) { con ->
            con.prepareStatement(sql).use { stmt ->
                preparator(stmt)
                stmt.execute()
            }
        }
    }

    override fun executeQuery(sql: String, preparator: (PreparedStatement) -> Unit, consumer: (ResultSet) -> Unit) {
        execute0(sql) { con ->
            con.prepareStatement(sql).use { stmt ->
                preparator(stmt)
                stmt.executeQuery().use { rs ->
                    while (rs.next()) {
                        consumer(rs)
                    }
                }
            }
        }
    }

    private fun <T> execute0(sql: String, code: (Connection) -> T): T {
        conLogger.log(sql)
        val autoCommit = con.autoCommit
        val res = code(con)
        check(con.autoCommit == autoCommit)
        return res
    }
}

class DatabaseConnectorSqlManager(private val connector: DatabaseConnector, logging: Boolean): SqlManager() {
    private val conLogger = SqlConnectionLogger(logging)

    override fun <T> execute0(tx: Boolean, code: (SqlExecutor) -> T): T {
        val res = if (tx) {
            connector.withWriteConnection { con ->
                executeWithConnection(con, false, code)
            }
        } else {
            connector.withReadConnection { con ->
                executeWithConnection(con, true, code)
            }
        }
        return res
    }

    private fun <T> executeWithConnection(con: Connection, autoCommit: Boolean, code: (SqlExecutor) -> T): T {
        check(con.autoCommit == autoCommit)
        val sqlExec = ConnectionSqlExecutor(con, conLogger)
        val res = code(sqlExec)
        check(con.autoCommit == autoCommit)
        return res
    }
}
